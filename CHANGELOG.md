# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [v0.6.2] - 2025-02-25

### Added
 - Floating create todo button in today and folder pages

### Changed
 - Effort indicator emoji to a circle

### Fixed
 - Snooze functionality on week view
 - Margin issues on modal input todo
 - Layout issues in smaller screen sizes
 - Fix modal overflowing on smaller screens

## [v0.6.1] - 2024-06-10

### Added
 - Prompt for user when updates are available

### Fixed
 - Small fixes in pt/es translations texts
 - Bug causing recurring tasks to not show on week/today pages

## [v0.6.0] - 2024-05-21

### Added
 - Automated tests on week navigation, translation and backups
 - Spanish translation
 - Portuguese translation (thanks to [Patricia Machado](readpatricia.com))

### Changed
 - Lists now render only 10 itens at a time, for performance reasons
 - Ticking a todo in a specific day view now ticks it as done in that day

### Fixed
- Bug with z-index of week navigation

## [v0.5.2] - 2024-05-16

### Fixed
 - Fixed heading levels and accessible labels

## [v0.5.1] - 2024-05-16

### Fixed
 - Removed broken columns in todo list in bigger screen sizes

## [v0.5.0] - 2024-05-16

### Added
 - Ability to change the done date of a task

### Changed
 - Overhauled layout and spacing for all pages
 - Menu now closes when clicking on a menu link
 - Using system-ui font-family

### Fixed
 - Ticking a task doesn't set a due date if it's null
 - Bug on scroll for main menu

## [v0.4.2] - 2024-02-13

### Fixed
 - Bug where some pages didn't render at all
 - Fixed some stupid code and learned more about vue

## [v0.4.1] - 2024-02-13

### Fixed
 - Fixed todo options on today page not showing

## [v0.4.0] - 2024-02-11

### Added
 - New home page with onboarding text and components
 - New overdue page that shows in menu if there are any overdue tasks
 - New sticker button
 - New logo/icon
 - Snooze recurring task functionality to today page
 - Set new due date button in today page
 - remove due date button in today page

### Removed
 - legacy header and footer

### Changed
 - Moved old home page to week page
 - Color palette

## [0.3.2] - 2023-12-31

### Added
 - PWA capabilities

## [0.3.1] - 2023-12-30

### Fixed
 - Fixed bug for empty localStorage not opening the app

## [0.3.0] - 2023-12-29

### Added
 - Redo/undo on ctrl+z and ctrl+y shortcuts
 - Tasks can now be in folders
 - CRUD for folders in config page
 - Backup functionality

### Changed
 - Updated vuex to pinia, with retrocompatibility on localStorage
 - Do not show recurring tasks for today that were already done (based on recurring days and task name)
 - Editing and creating a task inside a modal
 - Making date and recurring days params mutually exclusive
 - Changed label in new todo button from "done" to "save" to prevent confusion

### Fixed
- Fixed a very annoying bug where ticking a todo on a list and clicking "outside" of it, it was being unticked
 - Do not count recurring tasks on "planned for this day" warning
 - Fix bug tasks displaying the wrong date
 - Fixed weird keyboard navigation for the sidebar menu

## [0.2.0] - 2023-12-14

### Added
 - Context menu to make a todo recurring
 - Date indicator in lists that don't come for a date

### Changed
 - Increased button size to accommodate new date on lists
 - Migrated to vue 3 
 - Updated node version
 - Navigation on a new lateral menu

### Fixed
 - Ticks todo now adds today as the due date, so it's less confusing for the user when they untick it
 - Fixes focus bug causing ticking todo to intermitently not work

## [0.1.1] - 2023-11-24

### Added
 - Version info on store and config screen

## [0.1.0] - 2023-11-03

### Added

 - Basic todo list with effort based system
 - vuex-store with retrocompatibility with other non-tagged versions