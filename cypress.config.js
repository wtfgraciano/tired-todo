const { defineConfig } = require('cypress')

module.exports = defineConfig({
  pageLoadTimeout: 20_000,
  requestTimeout: 10_000,
  defaultBrowser: 'firefox',
  e2e: {
    specPattern: 'cypress/e2e/**/*.{cy,spec}.{js,jsx,ts,tsx}',
    baseUrl: 'http://127.0.0.1:4173'
  },
  component: {
    specPattern: 'src/**/__tests__/*.{cy,spec}.{js,ts,jsx,tsx}',
    devServer: {
      framework: 'vue',
      bundler: 'vite'
    }
  }
})
