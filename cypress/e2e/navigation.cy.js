// https://on.cypress.io/api

function hasMenu() {
  cy.contains('button', 'menu 📑')
}

describe('Menu navigation', () => {
  it('homepage has menu', () => {
    cy.visit('/')
    hasMenu()
  })

  it('has the menu on all pages', () => {
    cy.visit('/')
    cy.contains('button', 'menu 📑').click()
    cy.get('nav[aria-label="main"]')
      .should('be.visible')
      .find('li > a')
      .each(($el) => {
        cy.wrap($el).click()
        hasMenu()
        // opens menu again for next iteration
        cy.contains('button', 'menu 📑').click()
      })
  })
})
