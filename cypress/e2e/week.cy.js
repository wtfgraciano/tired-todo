// https://on.cypress.io/api

const { format, add, sub } = require('date-fns')

describe('Week page', () => {
  beforeEach(() => {
    cy.visit('/week', { timeout: 5_000 })
  })
  it('has all weekdays in headings', () => {
    const WEEKDAYS = [
      ('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday')
    ]
    WEEKDAYS.forEach((weekday) => {
      cy.contains('h2', weekday)
    })
  })
  it('has a previous button that navigates to the previous week', () => {
    cy.contains('a', 'Previous').click()
    const dayFromLastWeek = sub(new Date(), { weeks: 1 })
    cy.contains('h3', `${format(dayFromLastWeek, 'dd')}/${format(dayFromLastWeek, 'LLL')}`)
  })
  it('has a next button that navigates to the next week', () => {
    cy.contains('a', 'Next').click()
    const dayFromNextWeek = add(new Date(), { weeks: 1 })
    cy.contains('h3', `${format(dayFromNextWeek, 'dd')}/${format(dayFromNextWeek, 'LLL')}`)
  })
  it('has a this week button that navigates to this week', () => {
    const today = new Date()
    cy.contains('a', 'This week').click()
    cy.contains('h3', `${format(today, 'dd')}/${format(today, 'LLL')}`)
  })
})
