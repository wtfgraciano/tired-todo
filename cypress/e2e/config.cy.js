// https://on.cypress.io/api

describe('Config page', () => {
  beforeEach(() => {
    cy.clearAllLocalStorage()
    cy.visit('/config')
  })
  it('has effort limits', () => {
    cy.contains('h2', 'Effort limits')
  })
  it('has default chores folder', () => {
    cy.contains('h2', 'Folders')
    cy.get('li input').should('have.value', '🧹 Chores')
  })
  it('can create new folder', () => {
    cy.contains('label', 'New Folder').find('input').type('my-test-folder')
    cy.contains('button[type="submit"]', 'Save').click()
    cy.get('li:last-child input').should('have.value', 'my-test-folder')
  })
  it('changes lang to spanish', () => {
    cy.get('select[aria-label="Change language"]')
      .children('option[value=es]').should('exist')
    cy.get('select[aria-label="Change language"]')
      .select( 'Español')
    cy.contains('h2', 'Límites de Esfuerzo')
  })
  it('changes language to portuguese', () => {
    cy.get('select[aria-label="Change language"]')
      .children('option[value=pt]').should('exist')
    cy.get('select[aria-label="Change language"]').select('pt')
    cy.contains('h2', 'Limite de esforço')
  })
})
