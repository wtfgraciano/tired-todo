// https://on.cypress.io/api

describe('Homepage', () => {
  it('has the title in an h1', () => {
    cy.visit('/')
    cy.contains('h1', 'Ludista de Tarefas')
  })
  it('creates an undated task', () => {
    cy.visit('/')
    cy.contains('button', '➕').click()
    cy.get('input').first().type('new task')
    cy.contains('button', 'Save').click()
    cy.get('ul[data-test-id="todo-list"]').contains('li', 'new task')
  })
})
