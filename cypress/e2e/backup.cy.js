describe('Backup page', () => {
  beforeEach(() => {
    cy.clearAllLocalStorage()
    cy.visit('/config')
  })
  it('has a backup and a restore backup button', () => {
    cy.contains('button', '⬇️ Save Backup')
    cy.contains('button', '⬆️ View backup to recover')
  })
  it('can recover backup from v0.3.1 and before', () => {
    cy.get('input[type="file"]').selectFile('cypress/fixtures/v0.3.1-2024-28-17-backup.json')
    cy.contains('button', '⬆️ View backup to recover').click()
    cy.contains('h3', 'From version <not detected>')
    cy.contains('button', 'Delete database and recover from backup').click()
    cy.should('not.contain', 'From version <not detected>')
    cy.contains('div', 'Data overwritten with backup, refreshing now')
    cy.visit('/all')
    cy.contains('span', 'pagar contas')
  })
  it('can recover backup from v0.4.2', () => {
    cy.get('input[type="file"]').selectFile('cypress/fixtures/v0.4.2-2024-36-17-backup.json')
    cy.contains('button', '⬆️ View backup to recover').click()
    cy.contains('h3', 'From version 0.4.2')
    cy.contains('button', 'Delete database and recover from backup').click()
    cy.should('not.contain', 'h3', 'From version 0.4.2')
    cy.visit('/all')
    cy.contains('span', 'pagar contas')
    cy.get('nav[aria-label="main"]').contains('a', 'new folder')
  })
})
