import { fileURLToPath, URL } from 'node:url'

import { defineConfig } from 'vite'
import { VitePWA } from 'vite-plugin-pwa'
import vue from '@vitejs/plugin-vue'
import svgLoader from 'vite-svg-loader'
import * as child from 'child_process'

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    svgLoader(),
    VitePWA({
      registerType: 'prompt',
      name: 'Ludista de Tarefas',
      themeColor: '#4DBA87',
      msTileColor: '#000000',
      injectRegister: 'auto',
      workbox: {
        sourcemap: true
      }
    })
  ],
  define: {
    __COMMIT_HASH: JSON.stringify(child.execSync('git rev-parse --short HEAD').toString()),
    // eslint-disable-next-line no-undef
    __APP_VERSION: JSON.stringify(process.env.npm_package_version)
  },
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  }
})
