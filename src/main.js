import { createApp } from 'vue'
import { createRouter, createWebHistory } from 'vue-router'
import { APP_VERSION, COMMIT_HASH } from '@/services/constants'
import App from './App.vue'
import store from './store'
import routes from '@/services/routes'
import userLocale from '@/services/userLocale'
import en from '@/assets/locales/en.json'
import pt from '@/assets/locales/pt.json'
import es from '@/assets/locales/es.json'
import updateSW from '@/plugins/updateSW'

import * as log from 'loglevel'
import { createI18n } from 'vue-i18n'

const i18n = createI18n({
  legacy: false,
  locale: userLocale(),
  fallbackLocale: 'en',
  messages: { en, pt, es }
})

log.setLevel(import.meta.env.DEV ? 'debug' : 'warn')
log.info('env mode', import.meta.env.MODE)
log.info('app version', APP_VERSION)
log.info('commit hash', COMMIT_HASH)

const app = createApp(App)

const router = createRouter({
  history: createWebHistory(),
  routes
})
app.use(router)
app.use(store)
app.use(i18n)
// service workers break cypress:
//  https://filiphric.com/how-to-wait-for-page-to-load-in-cypress
if (import.meta.env.MODE !== 'test')
{
  app.use(updateSW)
}

log.debug('app', app)
log.debug('store', store)

app.mount('#app')
