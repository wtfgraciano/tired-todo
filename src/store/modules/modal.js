import { defineStore } from 'pinia'

export const useModalStore = defineStore('modal', {
  state: () => ({
    modals: {}
  }),
  getters: {
    open: (state) => (id) => state.modals[id]?.open
  },
  actions: {
    registerModal() {
      const id = crypto.randomUUID()
      this.modals[id] = { open: false }
      return id
    },
    unregisterModal(id) {
      delete this.modals[id]
    },
    openModal(id) {
      this.modals[id].open = true
    },
    closeModal(id) {
      this.modals[id].open = false
    }
  }
})
