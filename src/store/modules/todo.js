import { addDays, format, isAfter, parseISO } from 'date-fns'
import { DATE_FORMAT } from '@/services/constants'
import { sameDayFromString, sameWeekday, fromThisWeek } from '@/services/dateSameDay'
import * as log from 'loglevel'
import persistPiniaRetroCompatible from '../persistPiniaRetroCompatible'

const isSnoozed = (todo, date) =>
  todo.recurringSnoozed && todo.recurringSnoozed.includes(format(date, DATE_FORMAT))

export const useTodoStore = persistPiniaRetroCompatible('todo', {
  state: () => ({
    folders: [
      {
        id: crypto.randomUUID(),
        name: '🧹 Chores'
      },
      {
        id: crypto.randomUUID(),
        name: '🦺 Work'
      },
      {
        id: crypto.randomUUID(),
        name: '🪽 Personal'
      }
    ],
    /**
     * @typedef Todo
     * @type {{ doneDate: Date, done: boolean, id: string, name: string, recurringDays?: [string], folderId?: string}}
     */

    /**
     * @type [Todo]
     */
    todos: []
  }),
  getters: {
    inboxTodos: (state) =>
      state.todos
        .filter((todo) => !todo.done)
        .filter((todo) => !todo.date && !(todo.recurringDays?.length > 0))
        .filter(
          (todo) => !todo.folderId || !state.folders.map((f) => f.id).includes(todo.folderId)
        ),
    fromFolder: (state) => (folderId) => state.todos.filter((todo) => todo.folderId === folderId),
    todosFromDate: (state) => (date) => {
      return state.todos.filter(
        (todo) =>
          (todo.recurringDays &&
            todo.recurringDays.find((weekday) => sameWeekday(weekday, date))) ||
          sameDayFromString(todo.date, date)
      )
    },
    /**
     *
     * @param state
     * @returns {function(*): ?Todo}
     */
    getTodoById: (state) => (id) => {
      return state.todos.find((todo) => todo.id === id)
    },
    getTodoIndexBydId: (state) => (id) => {
      return state.todos.findIndex((todo) => todo.id === id)
    },
    getFolderIndexBydId: (state) => (id) => {
      return state.folders.findIndex((folder) => folder.id === id)
    },
    getFolderById: (state) => (id) => state.folders.find((folder) => folder.id === id),
    tiredFromDay: (state) => (date) => {
      return state.todos
        .filter((todo) => sameDayFromString(todo.doneDate, date))
        .filter(({ done }) => done)
        .reduce((tired, { tiring }) => tired + tiring, 0)
    },
    doneThisDay: (state) => (date) => {
      return state.todos.filter((todo) => todo.done && sameDayFromString(todo.doneDate, date))
    },
    tiredThisWeek(state) {
      return state.todos
        .filter((todo) => todo.done && fromThisWeek(todo.doneDate))
        .reduce((tired, { tiring }) => tired + tiring, 0)
    },
    availableThisDay() {
      return (date) => {
        const fromDate = this.todosFromDate(date)
        const doneThisDay = this.doneThisDay(date)
        return fromDate
          .filter(({ done }) => !done)
          .filter((todo) => {
            const isRecurringDoneToday =
              todo.recurringDays && doneThisDay.find(({ taskName }) => todo.taskName === taskName)
            return !isRecurringDoneToday && !isSnoozed(todo, date)
          })
      }
    },
    plannedThisDay() {
      return (date) =>
        [...this.doneThisDay(date), ...this.availableThisDay(date)].reduce(
          (tired, { tiring }) => tired + tiring,
          0
        )
    },
    snoozedOnDay() {
      return (date) => this.todosFromDate(date).filter((todo) => isSnoozed(todo, date))
    },
    overdueTasks(state) {
      return state.todos
        .filter(({ done }) => !done)
        .filter(({ date }) => date && isAfter(new Date().setHours(0, 0, 0, 0), parseISO(date)))
    }
  },

  actions: {
    _OVEWRITE_BACKUP(state) {
      this.todos = state.todos
      this.folders = state.folders
    },
    /**
     *
     * @param todo
     * @type Todo
     */
    ADD_TODO(todo) {
      this.todos = [...this.todos, todo]
    },
    DELETE_TODO(id) {
      this.todos = this.todos.filter((todo) => todo.id !== id)
    },
    updateTodo(id, todo) {
      const index = this.getTodoIndexBydId(id)
      const oldTodo = this.todos[index]
      this.todos[index] = { ...oldTodo, ...todo }
    },
    _tickTodoWithRecurringDays({ id, done, contextDate, ...remainingTodo }) {
      if (done) {
        this.DELETE_TODO(id)
      } else {
        const doneDate = format(contextDate ?? new Date(), DATE_FORMAT)
        this.ADD_TODO({
          done: true,
          doneDate,
          ...remainingTodo,
          id: crypto.randomUUID()
        })
      }
    },
    _tickTodoWithoutRecurringDays({ id, contextDate, done }) {
      const updateParams = { done: !done }
      updateParams.doneDate = done ? null : format(contextDate ?? new Date(), DATE_FORMAT)
      this.updateTodo(id, updateParams)
    },
    tickTodo(todo) {
      log.debug('vuex action tickTodo', todo)
      const { recurringDays } = todo
      if (recurringDays && recurringDays.length > 0) {
        this._tickTodoWithRecurringDays(todo)
      } else {
        this._tickTodoWithoutRecurringDays(todo)
      }
    },
    rewriteTodo(todo) {
      log.debug('vuex action rewriteTodo', todo)
      const index = this.getTodoIndexBydId(todo.id)
      this.todos[index] = { ...todo }
    },
    newTodo(todo) {
      const id = crypto.randomUUID()
      const newTodo = {
        ...todo,
        id
      }
      this.ADD_TODO(newTodo)
      log.debug('vuex action newTodo', newTodo)
      return newTodo
    },
    removeTodo(todo) {
      this.DELETE_TODO(todo.id)
      log.debug('vuex action removeTodo', todo.id, { ...todo })
      // dispatch('toast/showMessage', 'task removed', { root: true })
    },
    newFolder(folder) {
      this.folders.push({ id: crypto.randomUUID(), ...folder })
    },
    editFolder(folder) {
      const index = this.getFolderIndexBydId(folder.id)
      const oldFolder = this.folders[index]
      this.folders[index] = { ...oldFolder, ...folder }
    },
    removeFolder(id) {
      this.folders = this.folders.filter((folder) => folder.id !== id)
      // on cascade
      this.fromFolder(id).map((todo) => {
        delete todo.folderId
        this.rewriteTodo(todo)
      })
    },
    snooze(todoId, date = new Date()) {
      const todo = this.getTodoById(todoId)
      if (todo?.recurringDays?.length > 0) {
        todo.recurringSnoozed = [format(date, DATE_FORMAT), ...(todo.recurringSnoozed || [])]
      } else {
        todo.date = format(addDays(date, 1), DATE_FORMAT)
      }
      this.rewriteTodo(todo)
    },
    recoverSnoozes(date) {
      const formattedDate = format(date, DATE_FORMAT)
      for (const todo of this.todos) {
        const indexRecurringSnoozed = todo.recurringSnoozed?.findIndex((el) => el === formattedDate)
        if (isNaN(indexRecurringSnoozed) || indexRecurringSnoozed < 0) continue
        todo.recurringSnoozed.splice(indexRecurringSnoozed, 1)
        this.rewriteTodo(todo)
      }
    }
  }
})
