import { defineStore } from 'pinia'

export const useToastStore = defineStore('toast', {
  state: () => ({
    show: false,
    timeout: 0,
    message: ''
  }),
  undo: {
    disable: true,
    omit: ['show', 'timeout', 'message']
  },
  actions: {
    SET_SHOW(value) {
      this.show = value
    },
    SET_TIMEOUT(value) {
      this.timeout = value
    },
    SET_MESSAGE(value) {
      this.message = value
    },
    showMessage(message) {
      this.SET_MESSAGE(message)
      this.SET_SHOW(true)
      clearTimeout(this.timeout)
      const timeout = setTimeout(() => this.SET_SHOW(false), 5000)
      this.SET_TIMEOUT(timeout)
    }
  }
})
