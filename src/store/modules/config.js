import userLocale from '../../services/userLocale'
import persistPiniaRetroCompatible from '../persistPiniaRetroCompatible'
const locale = userLocale()

export const useConfigStore = persistPiniaRetroCompatible('config', {
  state: () => ({
    locale,
    maxTiredAcceptablePerWeek: 50,
    maxTiredAcceptable: 10
  }),
  actions: {
    _OVEWRITE_BACKUP(state) {
      this.maxTiredAcceptable = state.maxTiredAcceptable
      this.maxTiredAcceptablePerWeek = state.maxTiredAcceptablePerWeek
    },
    UPDATE_MAX_TIRED_PER_WEEK(value) {
      this.maxTiredAcceptablePerWeek = value
    },
    UPDATE_MAX_TIRED_PER_DAY(value) {
      this.maxTiredAcceptable = value
    },
    updateConfig({ maxTiredAcceptable, maxTiredAcceptablePerWeek }) {
      this.UPDATE_MAX_TIRED_PER_WEEK(maxTiredAcceptablePerWeek)
      this.UPDATE_MAX_TIRED_PER_DAY(maxTiredAcceptable)
    },
    setVersion(value) {
      this.version = value
    }
  }
})
