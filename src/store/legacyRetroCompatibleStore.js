import { APP_VERSION } from '@/services/constants'

const compose =
  (...fns) =>
  (x) =>
    fns.reduceRight((y, f) => f(y), x)

function changeTodosToCorrectModule(oldState) {
  const state = { ...oldState }
  if (state.todos) {
    state.todo = {
      ...state.todo,
      todos: state.todos
    }
    delete state.todos
  }
  return state
}

function addDoneDateToTodos(state) {
  if (!state.todo.todos) return state
  return {
    ...state,
    todo: {
      ...state.todo,
      todos: state.todo.todos.map((todo) => {
        if (todo.date && todo.done && !todo.doneDate) {
          return {
            ...todo,
            doneDate: todo.date
          }
        }
        return todo
      })
    }
  }
}

function addPackageVersion(state) {
  return {
    ...state,
    config: {
      ...state.config,
      version: state.config?.version || APP_VERSION
    }
  }
}

export default function (state) {
  if (!state) return {}
  return compose(addDoneDateToTodos, changeTodosToCorrectModule, addPackageVersion)(state)
}
