import removeDateFromRecurringTasks from './removeDateFromRecurringTasks'

export default function backupCompatibility(backupJson) {
  const convertedBackup = { ...backupJson }
  convertedBackup.todo.folders = [...(backupJson.todo?.folders ?? [])]
  convertedBackup.todo = removeDateFromRecurringTasks(convertedBackup.todo)
  return convertedBackup
}
