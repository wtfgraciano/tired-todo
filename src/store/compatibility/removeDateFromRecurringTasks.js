export default function (state) {
  return {
    ...state,
    todos:
      state?.todos?.map((todo) => {
        if (todo.recurringDays?.length > 0)
          return {
            ...todo,
            date: undefined
          }
        return todo
      }) ?? []
  }
}
