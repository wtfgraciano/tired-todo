import removeDateFromRecurringTasks from './compatibility/removeDateFromRecurringTasks'
import retroCompatibleStore from './legacyRetroCompatibleStore'
import { defineStore } from 'pinia'

const LEGACY_EMPTY_STATE = {
  toast: { show: false, timeout: 0, message: '' },
  todo: { todos: [] },
  config: { maxTiredAcceptablePerWeek: 50, maxTiredAcceptable: 10 }
}

const MODULES_PERSISTED = ['todo', 'config']

const legacyVuexStorageState = (moduleName) => {
  // compatibility from 0.2.0 onwards, when vuex was upgraded to pinia
  let legacyVuexStorage
  try {
    legacyVuexStorage = JSON.parse(localStorage.getItem('vuex'))
  } catch (_) {
    legacyVuexStorage = LEGACY_EMPTY_STATE
  }
  return retroCompatibleStore(legacyVuexStorage)[moduleName]
}

export function piniaPersistPlugin({ store }) {
  if (!MODULES_PERSISTED.includes(store.$id)) return
  store.$subscribe(() => {
    localStorage.setItem(store.$id, JSON.stringify(store.$state))
  })
}

export default function persistPiniaRetroCompatible(moduleName, storeDefinition) {
  const legacyState = legacyVuexStorageState(moduleName)
  let state
  try {
    state = JSON.parse(localStorage.getItem(moduleName))
  } catch (_) {
    state = false
  }
  state = state ?? legacyState
  if (moduleName === 'todo') {
    state = removeDateFromRecurringTasks(state)
  }
  return defineStore(moduleName, {
    ...storeDefinition,
    state: () => {
      return { ...storeDefinition.state(), ...state }
    }
  })
}
