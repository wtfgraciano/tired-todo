import { createPinia } from 'pinia'
import { piniaPersistPlugin } from './persistPiniaRetroCompatible'
import { PiniaUndo } from 'pinia-undo'

const pinia = createPinia()

pinia.use(piniaPersistPlugin)
pinia.use(PiniaUndo)

export default pinia
