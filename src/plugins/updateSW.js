import { registerSW } from 'virtual:pwa-register'
import { ref } from 'vue'

export default {
  install(app) {
    app.config.globalProperties.$needsRefresh = ref(false)
    const updateSW = registerSW({
      onNeedRefresh() {
        app.config.globalProperties.$needsRefresh.value = true
      },
      onOfflineReady() {}
    })
    app.config.globalProperties.$updateSW = () => {
      updateSW()
      app.config.globalProperties.$needsRefresh.value = false
    }
  }
}
