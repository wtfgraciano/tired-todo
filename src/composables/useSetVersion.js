import { useConfigStore } from '@/store/modules/config'
import { APP_VERSION } from '@/services/constants'
import { onMounted } from 'vue'

export default function useSetVersion() {
  const store = useConfigStore()

  onMounted(() => {
    const storedVersion = store.version
    if (storedVersion !== APP_VERSION) {
      store.setVersion(APP_VERSION)
    }
  })
}
