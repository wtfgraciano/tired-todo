import { useTodoStore } from '@/store/modules/todo'
export default function useUndo() {
  const store = useTodoStore()
  window.addEventListener('keydown', (ev) => {
    if (!ev.ctrlKey && !ev.metaKey) return
    const letterPressed = ev.key.toLowerCase()
    if (ev.shiftKey && letterPressed == 'z') {
      store.redo()
    } else if (letterPressed === 'z') {
      store.undo()
    } else if (letterPressed === 'y') {
      store.redo()
    }
  })
}
