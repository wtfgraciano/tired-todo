import { useConfigStore } from '@/store/modules/config'
import { setDefaultOptions } from 'date-fns'
import { enUS, es, ptBR } from 'date-fns/locale'
import { watch, computed, onBeforeMount } from 'vue'
import { useI18n } from 'vue-i18n'

const localeToDateFnsLocale = {
  en: enUS,
  pt: ptBR,
  es: es
}

export default function useGlobalLocale() {
  const store = useConfigStore()
  const { locale } = useI18n({ useScope: 'global' })
  const configLocale = computed(() => store.locale)

  onBeforeMount(() => {
    // unforntunately I couldn't figure out how to read the store state in
    // main.js, so this is to prevent reloading the app always changing it
    // back to English again.
    locale.value = store.locale
    setDefaultOptions({ locale: localeToDateFnsLocale[store.locale] })
  })

  watch(configLocale, (newLocale) => {
    locale.value = newLocale
    setDefaultOptions({ locale: localeToDateFnsLocale[newLocale] })
  })
}
