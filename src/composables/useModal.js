import { onUnmounted } from 'vue'
import { useModalStore } from '@/store/modules/modal'
export default function useModal() {
  const modalStore = useModalStore()
  const modalId = modalStore.registerModal()
  onUnmounted(() => modalStore.unregisterModal(modalId))
  return {
    modalId,
    openModal: () => modalStore.openModal(modalId),
    closeModal: () => modalStore.closeModal(modalId)
  }
}
