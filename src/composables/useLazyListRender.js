import { onBeforeUpdate, ref, watchEffect } from 'vue'

export default function useLazyListRender({ eachStepItemsToRender, rootMargin }) {
  const numberOfElementsToRender = ref(eachStepItemsToRender)
  const itemsRef = ref([])

  onBeforeUpdate(() => {
    itemsRef.value = []
  })

  const observerHandler = (entries, observer) => {
    if (entries[0].isIntersecting) {
      numberOfElementsToRender.value = numberOfElementsToRender.value + eachStepItemsToRender
      observer.disconnect()
    }
  }

  watchEffect(
    () => {
      const nextElementToObserve = itemsRef.value[numberOfElementsToRender.value]
      if (!nextElementToObserve) return
      const observer = new IntersectionObserver(observerHandler, {
        rootMargin
      })
      observer.observe(nextElementToObserve)
    },
    {
      flush: 'post'
    }
  )
  return {
    shouldRenderElement: (index) => index < numberOfElementsToRender.value,
    itemsRef
  }
}
