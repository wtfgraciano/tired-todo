// eslint-disable-next-line no-undef
export const APP_VERSION = __APP_VERSION
// eslint-disable-next-line no-undef
export const COMMIT_HASH = __COMMIT_HASH
export const DATE_FORMAT = 'yyyy-MM-dd'
export const FOCUSABLE_SELETOR =
  'a, button, input, textarea, select, details,[tabindex]:not([tabindex="-1"])'
