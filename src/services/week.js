import {
  add,
  nextFriday,
  nextMonday,
  nextSaturday,
  nextSunday,
  nextThursday,
  nextTuesday,
  nextWednesday,
  startOfWeek
} from 'date-fns'

export function daysFromWeek(date) {
  const start = startOfWeek(date)
  const thisWeek = []
  for (let i = 0; i < 7; i++) {
    thisWeek.push(add(start, { days: i }))
  }
  return thisWeek
}

export const WEEKDAYS = ['mon', 'tue', 'wed', 'thu', 'fri', 'sat', 'sun']
export const WORKDAYS = ['mon', 'tue', 'wed', 'thu', 'fri']

const formatWeekDay = (date) =>
  date.toLocaleString('en-US', {
    weekday: 'short'
  })

export function localeWeekDay(weekday) {
  const strWeekdayToDate = {
    mon: formatWeekDay(nextMonday(new Date())),
    tue: formatWeekDay(nextTuesday(new Date())),
    wed: formatWeekDay(nextWednesday(new Date())),
    thu: formatWeekDay(nextThursday(new Date())),
    fri: formatWeekDay(nextFriday(new Date())),
    sat: formatWeekDay(nextSaturday(new Date())),
    sun: formatWeekDay(nextSunday(new Date()))
  }
  return strWeekdayToDate[weekday]
}
