import HomePage from '@/components/pages/HomePage.vue'
import TodayPage from '@/components/pages/TodayPage.vue'
import WeekPage from '@/components/pages/WeekPage.vue'
import AllTodos from '@/components/pages/AllTodos.vue'
import InboxPage from '@/components/pages/InboxPage.vue'
import FolderPage from '@/components/pages/FolderPage.vue'
import OverduePage from '@/components/pages/OverduePage.vue'
import Config from '@/components/pages/ConfigPage.vue'

export default [
  { path: '', component: HomePage },
  { path: '/inbox', component: InboxPage },
  { path: '/overdue', component: OverduePage },
  { path: '/today', component: TodayPage },
  { path: '/week', component: WeekPage },
  { path: '/week/:date', component: WeekPage },
  { path: '/f/:folderId/:folderName', component: FolderPage, props: true },
  { path: '/all', component: AllTodos },
  { path: '/config', component: Config }
]
