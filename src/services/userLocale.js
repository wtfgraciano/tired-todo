export const SUPPORTED_LOCALES = ['en', 'es', 'pt']

export default function userLocale() {
  const locale = SUPPORTED_LOCALES.find((lang) =>
    (navigator.languages ?? []).find((supportedLang) => lang.match(supportedLang))
  )
  return locale ?? 'en'
}
