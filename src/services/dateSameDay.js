import { isSameDay, isSameWeek, format, isValid, parseISO } from 'date-fns'
import { enUS } from 'date-fns/locale'

export function sameDayFromString(strDay, b) {
  return isSameDay(parseISO(strDay), b)
}

export function fromThisWeek(strDay) {
  return isSameWeek(parseISO(strDay), new Date())
}

export function sameWeekday(strDay, b) {
  if (!isValid(new Date(b))) return false
  return format(b, 'iii', { locale: enUS }).toLowerCase() === strDay
}
