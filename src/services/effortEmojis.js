export const EMOJIS = {
  1: '🙂',
  2: '😐​',
  3: '🫤',
  5: '☹️',
  8: '😢',
  13: '😭',
  20: '💀'
}

export const EMOJI_SIZES = {
  BIG: 'big',
  MEDIUM: 'medium',
  SMALL: 'small'
}

export const chooseEmoji = (number) => {
  if (number > 20) return '🔥'
  return EMOJIS[number] ?? chooseEmoji(number + 1)
}

export const emojiSize = (number) => {
  if (number >= 20) return EMOJI_SIZES.BIG
  if (number >= 5) return EMOJI_SIZES.MEDIUM
  return EMOJI_SIZES.SMALL
}
