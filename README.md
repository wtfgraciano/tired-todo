# Ludista de Tarefas

This is a local first (currently local only) anti-productivity todo list. Instead of having productivity goals you have effort limits to not exert yourself. Go get some rest. Reject productivity ideology. Sleep when they're working. Being organized is not the same as being productive ;) The vision for this project is a little more elaborated at https://ludistadetarefas.net. The app itself is available at https://web.ludistadetarefas.net/.

## Contributing

If you are considering contributing to this project, thanks! First of all, this project is in vue 3 and I tried my best to give setup instructions junior developers can actually follow. If you find any problem with it, please create an issue or even an MR to improve it.

### Contributing without writing any code

All merges to main go to https://beta.ludistadetarefas.net, and you can go there try the new features before they're released, and if you find any bugs, create an issue here. Note that the development version will have a different "database" for your todos, since they are stored locally based on the domain.

### Contributing with code

If you want to create an MR that creates new features/components, I kindly ask that you write tests for them, but it's completely okay if you don't know how to and can't write them. It's not a requirement for contributing.

### Project local development setup

You'll need to install nodejs. I recommend using [asdf](https://asdf-vm.com/) and the [node plugin](https://github.com/asdf-vm/asdf-nodejs). It gets the default version from the `.nvmrc` file in this project.

Follow [this guide](https://asdf-vm.com/guide/getting-started.html#_3-install-asdf) to install asdf. Then, add the nodejs plugin: `asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git`

After that, clone this repo (or clone the fork you can create on gitlab), cd into the created directory, and install the supported nodejs version:
```
asdf install nodejs $(cat .nvmrc)
```

Then, install the dependencies normally.
```
npm install
```

#### Compiles and hot-reloads for development

This command will give you the localhost link you can use to test your development version locally.
```
npm run dev
```

#### Here are some of the things you might want to check in your text editor / IDE
 - https://editorconfig.org/
 - https://eslint.org/
 - https://prettier.io/

#### Other relevant commands:

##### Compiles and minifies for production
```
npm run build
```
##### Build for testing e2e
```shell
npm run build:test
```

##### Run the tests

Note that you need to build (`build:test` command) the project first, so it'll generate the version to be e2e tested on cypress.
```
npm run test:e2e
```

##### To regenerate the icons from a new source logo, run
```
npx @vite-pwa/assets-generator --preset minimal-2023 public/new-source-logo.svg
```

##### Lints and fixes files
```
npm run lint
```
## License

This project is under The Prosperity Public License 3.0.0, as can be seen on the LICENSE file, and NOT any other License as gitlab may currently automaticaly say.
